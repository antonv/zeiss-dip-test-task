import { expect } from 'chai';
import sut from './machinesStreamService';

const expectProperStatus = (status) =>
  expect(status).to.be.oneOf(['idle', 'running', 'finished', 'errorred', 'repaired']);

describe('machinesStreamService', () => {
  describe('get machines', () => {
    it('should return list of installed machines and no errors', async () => {
      const res = await sut.getMachines();
      expect(res).to.be.an('array');
      expectProperStatus(res[0].status);
    });
  });
  describe('getMachineEvents', () => {
    let machines = null;
    beforeEach(() => sut.getMachines()
      .then(res => machines = res));
    it('should return machine details with latest events', async () => {
      const machineId = machines[0].id;
      const res = await sut.getLastMachineEvents(machineId);
      expect(res.id).to.equal(machineId);
      console.log('machine events', res);
    });
  });
});