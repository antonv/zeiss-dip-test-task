
export const post = (url, body) => {
  return fetch(url, {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then((res) => {
      if (!res.ok) {
        const err = new Error(`${res.status} - ${res.statusText} - ${res.message}`);
        err.status = res.status;
        throw err;
      }
      return res.json();
    });
}

export const get = (url) => {
  return fetch(url, {
    method: 'GET',
    credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
    },
  })
    .then((res) => {
      if (!res.ok) {
        const err = new Error(`${res.status} - ${res.statusText}`);
        err.status = res.status;
        throw err;
      }
      return res.json();
    });
}

export const put = (url, body) => {
  return fetch(url, {
    method: 'PUT',
    credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  })
    .then((res) => {
      if (!res.ok) {
        const err = new Error(`${res.status} - ${res.statusText}`);
        err.status = res.status;
        throw err;
      }
      return res.json();
    });
}
