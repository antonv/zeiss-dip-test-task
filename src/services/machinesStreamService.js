import { Socket } from "phoenix";
import { get } from './httpService';

const baseUrl = 'https://machinestream.herokuapp.com/api/v1';
const baseWsUrl = baseUrl.replace('https://', 'ws://');

const getMachines = () => get(`${baseUrl}/machines`).then(res => res.data);

const getLastMachineEvents = machineId =>
  get(`${baseUrl}/machines/${machineId}`).then(res => res.data);

const subscribeToEvents = (eventHandler) => {
  if (typeof eventHandler !== 'function') {
    throw new Error('Provide event handler function as 1st argument');
  }
  const wsUrl = `${baseWsUrl}/events`;
  // Open Socket connection
  const socket = new Socket(wsUrl);
  socket.connect();

  // Join correct channel and log events
  const channel = socket.channel("events", {});
  channel.on("new", event => {
    console.log(`New event on ${wsUrl}:`, event);
    eventHandler(event); 
  });
  channel.join();

  const unsubscribe = () => {
    console.log(`Closing socket ${wsUrl}`);
    channel.leave("events");
    socket.disconnect();
  };

  return unsubscribe;
}

export default {
  getMachines,
  getLastMachineEvents,
  subscribeToEvents,
};
