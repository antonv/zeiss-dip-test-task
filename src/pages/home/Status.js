import React from 'react';
import PropTypes from 'prop-types';

const renderStatus = status => {
  switch (status) {
    case 'idle':
      return '❔';
    case 'finished':
      return '✔️';
    case 'running':
      return '⚙️';
    case 'errored':
      return '❌';
    case 'repaired':
      return '🔨';
    default:
      return status;
  }
};
const Status = ({ status }) => {
  return (
    <span>
      {renderStatus(status)}
    </span>
  );
};

Status.propTypes = {
  status: PropTypes.string.isRequired,
};

export default Status;