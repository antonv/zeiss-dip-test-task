import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Status from './Status';

const renderEvent = (event) => {
  return (
    <li className="list-group-item" key={event.id}>
    <code>
      {`${moment(event.timestamp).format('HH:mm:ss')}: machine ${event.machine_id}`}
      {' changed status to '}
      <Status status={event.status} />
      {` (${event.status})`}
    </code>
    </li>
  );
}

const EventLog = ({ events }) => {
  return (
    <div>
      <ul className="list-group">
        {events.map(ev => renderEvent(ev))}
      </ul>
    </div>
  );
};

EventLog.propTypes = {
  events: PropTypes.array.isRequired,
}

export default EventLog;