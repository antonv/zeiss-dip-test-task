import React, { Component } from 'react';
import moment from 'moment';
import machinesStreamService from '../../services/machinesStreamService';
import './Home.css';

import EventLog from './EventLog';
import Status from './Status';

const fakeEvent = false;

class Home extends Component {
  state = {
    machines: null,
    highlightedMachines: {},
    events: [],
  };

  async componentDidMount() {
    await this.loadMachines();
    if (fakeEvent) {
      setTimeout(() => {
        const firstMachine = this.state.machines[0];
        this.handleMachineStreamEvent({ machine_id: firstMachine.id, status: 'errored'});
      }, 1000);
    } else {
      this.unsubscribeFromEvents = machinesStreamService.subscribeToEvents(
        this.handleMachineStreamEvent
      );
    }
  }

  loadMachines = async () => {
    const machines = await machinesStreamService.getMachines();
    this.setState({ machines });
  };

  componentWillUnmount() {
    if (this.unsubscribeFromEvents) {
      this.unsubscribeFromEvents();
    }
  }

  handleMachineStreamEvent = event => {
    const { machines } = this.state;
    this.setState(state => ({
      events: [event, ...state.events],
    }))
    if (!machines || !machines.length) {
      this.loadMachines();
      return null;
    }
    const targetMachine = machines.find(m => m.id === event.machine_id);
    console.log('!!!!!!', targetMachine, event);
    if (!targetMachine) {
      this.loadMachines();
      return null;
    }

    const updatedMachines = machines.slice(0);
    updatedMachines[machines.indexOf(targetMachine)] = {
      ...targetMachine,
      status: event.status,
      highlighted: true
    };
    this.setState({ machines: updatedMachines });
    this.highlightMachine(event.machine_id);
  };

  highlightMachine = (machineId) => {
    this.setState(state => ({
      highlightedMachines: {
        ...state.highlightedMachines,
        [machineId]: true,
      }
    }));
    setTimeout(() => this.resetMachineHighlight(machineId), 2100);
}

  resetMachineHighlight = machineId => {
    this.setState(state => ({
      highlightedMachines: {
        ...state.highlightedMachines,
        [machineId]: false,
      }
    }));
  };

  renderPosition = machine => {
    const gMapsUrl = `http://www.google.com/maps/place/${machine.latitude},${machine.longitude}`;
    return (
      <a href={gMapsUrl} target="blank">
        Floor #{machine.floor}
      </a>
    );
  };

  renderMachines() {
    const { machines, highlightedMachines } = this.state;
    if (machines === null) {
      return (
        <div className="alert alert-light" role="alert">
          Loading ...
        </div>
      );
    }

    if (machines && !machines.length) {
      return (
        <div className="alert alert-info" role="alert">
          No machines yet sent.
        </div>
      );
    }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Status</th>
            <th scope="col">Type</th>
            <th scope="col">Installed</th>
            <th scope="col">Last maintenance</th>
            <th scope="col">Position</th>
          </tr>
        </thead>
        <tbody>
          {machines.map(machine => (
            <tr
              key={machine.id}
              className={`HomePage-machineRow ${highlightedMachines[machine.id] ? 'highlighted': ''}`}
            >
              <td><Status status={machine.status} /></td>
              <td>{machine.machine_type}</td>
              <td>{machine.install_date}</td>
              <td>{moment(machine.last_maintenance, moment.ISO_8601).fromNow()}</td>
              <td>{this.renderPosition(machine)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  render() {
    const { events } = this.state;
    return (
      <div className="HomePage">
      <div className="row">
        <div className="col-8 pl-5">
          <h1>Installed machines</h1>
          {this.renderMachines()}
        </div>
        <div className="col-4 px-5">
          <h1>Events Log</h1>
          <EventLog events={events} />
        </div>
      </div>
      </div>
    );
  }
}

export default Home;
