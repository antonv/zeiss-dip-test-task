import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';
import Home from '../pages/home/Home';

const AppRouter = () => {
  return (
    <Router>
      <div>
        <header>
          <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <Link className="navbar-brand" to="/">
              Monitoring App
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarCollapse"
              aria-controls="navbarCollapse"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarCollapse">
              <ul className="navbar-nav mr-auto">
                <ActiveLink to="/" label="Home" />
                <ActiveLink to="/about" label="About" />
              </ul>
            </div>
          </nav>
        </header>
        <main role="main" className="container-fluid">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Redirect from="/home" to="/" />
            <Route component={NotFound} />
          </Switch>
        </main>
        <footer className="footer">
          <div className="container">
            <span className="text-muted">Test task for Zeiss DI. anton.vasilenko (c)</span>
          </div>
        </footer>
      </div>
    </Router>
  );
};

const ActiveLink = ({ label, to }) => {
  return (
    <Route
      path={to}
      exact
      children={({ match }) => (
        <li className={`nav-item ${match ? 'active' : ''}`}>
          <Link className="nav-link" to={to}>
            {label}
          </Link>
        </li>
      )}
    />
  );
};

const About = () => {
  return (
    <div>
      <h1>About</h1>
    </div>
  );
};

const NotFound = ({ location }) => (
  <div>
    <h1>Page {location.pathname} not found</h1>
    <h2>Try go browser back.</h2>
  </div>
);

export default AppRouter;
